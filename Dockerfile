# Stage 0: prepare node alpine image with package.json files and production node_modules
FROM node:12.19.1-alpine3.12@sha256:3ae30348acd445501758896f691106cbc32111f3525651c7256a7df75aa8a97d AS base
WORKDIR /usr/src/app
COPY package.json yarn.lock /usr/src/app/
COPY ./packages/tds-backend/package.json /usr/src/app/packages/tds-backend/package.json
COPY ./packages/tds-frontend/package.json /usr/src/app/packages/tds-frontend/package.json
RUN yarn install --frozen-lockfile --silent --ignore-optional --production && yarn cache clean

# Stage 1: build frontend
FROM base AS builder
WORKDIR /usr/src/app
# Install dev dependencies as well
RUN yarn install --frozen-lockfile --silent --ignore-optional
# Import ARGs
ARG REACT_APP_ENV
ARG REACT_APP_API_TDS
ARG PUBLIC_URL
# Build frontend
COPY ./packages/tds-frontend /usr/src/app/packages/tds-frontend
RUN cd packages/tds-frontend && yarn build

# Stage 3: run light app
FROM base
WORKDIR /usr/src/app
COPY ./packages/tds-backend /usr/src/app/packages/tds-backend
COPY --from=builder /usr/src/app/packages/tds-frontend/build /usr/src/app/packages/tds-backend/public
RUN mkdir -p /usr/src/app/packages/tds-backend/peerOrganizations/ebsinode1/tls/tlsintermediatecerts \
  && mkdir -p /usr/src/app/packages/tds-backend/peerOrganizations/ebsinode2/tls/tlsintermediatecerts \
  && mkdir -p /usr/src/app/packages/tds-backend/peerOrganizations/ebsinode3/tls/tlsintermediatecerts \
  && mkdir -p /usr/src/app/packages/tds-backend/peerOrganizations/ebsinode1/users/Admin01@ebsi.xyz/msp/keystore \
  && mkdir -p /usr/src/app/packages/tds-backend/peerOrganizations/ebsinode1/users/Admin01@ebsi.xyz/msp/admincerts
RUN chown -R node:node /usr/src/app
USER node
CMD node /usr/src/app/packages/tds-backend/src/start.js

pipeline {
    agent any
    options {
        skipDefaultCheckout(true)
    }
    environment {
        EBSI_ENV="local"
        TDS_API_USERNAME=credentials('TDS_API_USERNAME')
        TDS_API_PASSWORD=credentials('TDS_API_PASSWORD')
        PORT='8180'
        peerOrganizations=credentials('peerOrganizations')
        attributes=credentials('attributes')
    }
    stages {
        stage('Clone repo') {
            steps {
                cleanWs()
                checkout scm;
            }
        }
        stage('Certs') {
            steps{
                sh 'cp $peerOrganizations $WORKSPACE'
                fileOperations([fileUnZipOperation(filePath: 'peerOrganizations.zip', targetLocation: 'packages/tds-backend')])
                sh 'cp $attributes $WORKSPACE'
                fileOperations([fileUnZipOperation(filePath: 'attributes.zip', targetLocation: 'packages/tds-backend')])
            }
        }
        stage('Install') {
            steps{
                sh 'yarn install --frozen-lockfile'
            }
        }
        stage('Unit Test') {
            steps{
                sh 'yarn run test:unit'
            }
        }
        stage('SonarQube Analysis') {
            steps {
                withSonarQubeEnv('LocalSonar'){
                    sh "/var/lib/jenkins/tools/hudson.plugins.sonar.SonarRunnerInstallation/sonar-scanner/bin/sonar-scanner -Dsonar.host.url=https://infra.ebsi.xyz/sonar -Dsonar.projectName=trusted-data-sharing -Dsonar.projectVersion=1.0 -Dsonar.projectKey=trusted-data-sharing -Dsonar.sources=. -Dsonar.projectBaseDir=${WORKSPACE}"
                }
            }
        }
        stage("Quality Gate") {
            steps {
                waitForQualityGate abortPipeline: true
            }
        }
    }
    post {
        always {
            cleanWs()
            dir("${env.WORKSPACE}@script") {
                deleteDir()
            }
        }
    }
}

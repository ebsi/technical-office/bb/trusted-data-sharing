const cutils = require("./cutils");
const config = require("./config");
const logger = require("./logger");

const userName = config.username;

async function createIossVat(data) {
  logger.info("Creating Public and Private IossVat models");

  // <<AH>> Hardcoded values should not be in V2.
  const request = {
    chaincodeId: "taxudchaincode",
    fcn: "createIossVat",
    args: [data.sharedID, data.iossVatID, data.startDate, data.endDate],
    chainId: "ebsichannel",
  };

  // <<AH>> TODO! Fix the error handling -- else is hard to debug
  const result = await cutils.invokeChaincode(userName, request);
  logger.info("[*] Success", result);
  return "";
}

async function updateIossVatPublicData(data) {
  logger.info("Updating existing public VatDetails.");

  const request = {
    chaincodeId: "taxudchaincode",
    fcn: "updateIossVatPublicData",
    args: [data.sharedID, data.startDate, data.endDate],
    chainId: "ebsichannel",
  };

  const result = await cutils.invokeChaincode(userName, request);
  logger.info("[*] Success", result);
  return "";
}

async function updateIossVatPrivateData(data) {
  logger.info("Updating existing private VatDetails.");

  const request = {
    chaincodeId: "taxudchaincode",
    fcn: "updateIossVatPrivateData",
    args: [data.sharedID, data.iossVatID],
    chainId: "ebsichannel",
  };

  const result = await cutils.invokeChaincode(userName, request);
  logger.info("[*] Success", result);
  return "";
}

async function verifyIossVatID(data) {
  logger.info("verifying IossVatID");

  const request = {
    chaincodeId: "taxudchaincode",
    fcn: "verifyIossVatID",
    args: [data.sharedID, data.iossVatID],
    chainId: "ebsichannel",
  };

  const result = await cutils.invokeChaincode(userName, request);
  logger.info("[*] Success", result);
  return "";
}

async function queryAllIossVatPublicData() {
  logger.info("getting all VatDetails from ledger: ");

  const request = {
    chaincodeId: "taxudchaincode",
    txId: null,
    fcn: "queryAllIossVatPublicData",
    args: [""],
  };

  const result = await cutils.invokeChaincode(userName, request);
  logger.info("[*] Success", result);
  return JSON.parse(result.toString());
}

async function queryAllIossVatPrivateData() {
  logger.info("getting all VatDetails from ledger: ");

  const request = {
    chaincodeId: "taxudchaincode",
    txId: null,
    fcn: "queryAllIossVatPrivateData",
    args: [""],
  };
  const result = await cutils.invokeChaincode(userName, request);
  logger.info("[*] Success", result);
  return JSON.parse(result.toString());
}

async function queryIossVatPublicDataBySharedId(data) {
  logger.info("getting specific public IOSS details");

  const request = {
    chaincodeId: "taxudchaincode",
    fcn: "queryIossVatPublicDataBySharedID",
    args: [data.sharedID],
    chainId: "ebsichannel",
  };
  const result = await cutils.invokeChaincode(userName, request);
  logger.info("[*] Success", result);
  return JSON.parse(result.toString());
}

async function queryIossVatPrivateDataBySharedId(data) {
  logger.info("getting specific private IOSS details");

  const request = {
    chaincodeId: "taxudchaincode",
    fcn: "queryIossVatPrivateDataBySharedID",
    args: [data.sharedID],
    chainId: "ebsichannel",
  };

  const result = await cutils.invokeChaincode(userName, request);
  logger.info("[*] Success", result);
  return JSON.parse(result.toString());
}

module.exports = {
  createIossVat,
  updateIossVatPublicData,
  updateIossVatPrivateData,
  verifyIossVatID,
  queryAllIossVatPublicData,
  queryAllIossVatPrivateData,
  queryIossVatPublicDataBySharedId,
  queryIossVatPrivateDataBySharedId,
};

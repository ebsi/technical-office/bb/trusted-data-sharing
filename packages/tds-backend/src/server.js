const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const logger = require("./logger");
const auth = require("./auth");
const errors = require("./errors");
const taxudAPI = require("./router");

require("./wallet").init();

class App {
  constructor() {
    this.httpServer = express();

    this.httpServer.options("*", cors());
    this.httpServer.use(cors());
    this.httpServer.use(
      bodyParser.urlencoded({
        extended: true,
      })
    );
    this.httpServer.use(bodyParser.json());

    this.httpServer.use((req, res, next) => {
      logger.info(`${req.method} ${req.url}`);
      next();
    });

    this.httpServer.use(
      "/demo/trusted-data-sharing",
      express.static(`${__dirname}/../public`)
    );

    this.httpServer.use("/", auth.handleToken);
    this.httpServer.use("/tds/v1/iossvat", taxudAPI);

    this.httpServer.use((req, res, next) => {
      next(
        new errors.BadRequestError(`Invalid service '${req.method} ${req.url}'`)
      );
    });

    this.httpServer.use(errors.handler);
  }

  getServer() {
    return this.httpServer;
  }

  start(port, testMode = false) {
    return this.httpServer.listen(port, () => {
      logger.info(`Trusted Data Sharing API started at port ${port}`);
      if (testMode) logger.info("EBSI TEST MODE enabled");
    });
  }
}

module.exports = App;

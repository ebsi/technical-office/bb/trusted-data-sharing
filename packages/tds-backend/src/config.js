const fs = require("fs");
const path = require("path");

const logger = require("./logger");
require("dotenv").config();

const port = process.env.PORT || 8080;

if (!process.env.TDS_API_USERNAME)
  throw new Error("TDS_API_USERNAME is not defined");
if (!process.env.TDS_API_PASSWORD)
  throw new Error("TDS_API_PASSWORD is not defined");

const username = process.env.TDS_API_USERNAME;
const password = process.env.TDS_API_PASSWORD;

const config = {
  production: {
    url: "https://api.ebsi.tech.ec.europa.eu",
    fabricDomain: "fabric-prod.ebsi.tech.ec.europa.eu",
  },

  development: {
    url: "https://api.ebsi.xyz",
    fabricDomain: "fabric-dev.ebsi.xyz",
  },

  integration: {
    url: "https://api.intebsi.xyz",
    fabricDomain: "0-fabric-int-lux.intebsi.xyz",
  },

  local: {
    url: "https://api.intebsi.xyz",
    fabricDomain: "0-fabric-int-lux.intebsi.xyz",
  },
};

const environment = process.env.EBSI_ENV || "development";
const finalConfig = config[environment];
const { url } = finalConfig;

const sharedConf = {
  walletPath: path.resolve(__dirname, "../attributes/adminWallet"),
  peerOrgsPath: path.resolve(__dirname, "../peerOrganizations"),
  channelName: "ebsichannel",
  jwt: {
    acceptedApp: "ebsi-wallet",
  },
  trustedAppsRegistry: `${url}/trusted-apps-registry/v1`,
  testMode: process.env.EBSI_TEST_MODE === "true",
};

const channel = { orderers: [], peers: {} };
const organizations = {};
const peers = {};
const certificateAuthorities = {};
const orderers = {};
const client = {
  tlsEnable: true,
  adminUser: username,
  adminPassword: password,
  enableAuthentication: false,
  organization: "EbsiNode1",
  connection: {
    timeout: {
      peer: { endorser: "300" },
      orderer: "300",
    },
  },
};

const { peerOrgsPath } = sharedConf;
const { fabricDomain } = finalConfig;

const NODE_ENROLLMENT = process.env.NODE_ENROLLMENT || 1;
const NODE_CONNECTION = process.env.NODE_CONNECTION || 2;

for (let i = 1; i <= 3; i += 1) {
  const ordererName = `orderer0${i}-${fabricDomain}`;
  const peerName = `peer0${i}-${fabricDomain}`;
  const organizationName = `EbsiNode${i}`;
  const certAuthority = `MspCa0${i}-${fabricDomain}`;

  if (i === NODE_ENROLLMENT) {
    organizations[organizationName] = {
      mspid: `EbsiNode${i}MSP`,
      peers: [peerName],
      certificateAuthorities: [certAuthority],
      adminPrivateKey: {
        path: `${peerOrgsPath}/ebsinode${i}/users/Admin0${i}@ebsi.xyz/msp/keystore/client_sk`,
      },
      signedCert: {
        path: `${peerOrgsPath}/ebsinode${i}/users/Admin0${i}@ebsi.xyz/msp/admincerts/cert.pem`,
      },
    };

    certificateAuthorities[certAuthority] = {
      url: `http://${certAuthority.toLowerCase()}:7055`,
      caName: certAuthority.toLowerCase(),
      registrar: {
        enrollId: "",
        enrollSecret: "",
      },
    };
  }

  if (i === NODE_CONNECTION) {
    const pathPeerCertificate = `${peerOrgsPath}/ebsinode${i}/tls/tlsintermediatecerts/tlsCa.pem`;

    peers[peerName] = {
      tlsCACerts: { path: pathPeerCertificate },
      url: `grpcs://${peerName}:7051`,
      eventUrl: `grpcs://${peerName}:7053`,
    };

    orderers[ordererName] = {
      tlsCACerts: { path: pathPeerCertificate },
      url: `grpcs://${ordererName}:7050`,
    };

    channel.orderers.push(ordererName);
    channel.peers[peerName] = {
      ledgerQuery: true,
    };

    // Verify that the certificates exist
    if (!fs.existsSync(pathPeerCertificate)) {
      logger.error(`Warning: The file ${pathPeerCertificate} does not exist`);
    }
  }
}

const channels = {
  ebsichannel: JSON.parse(JSON.stringify(channel)),
  testchannel: JSON.parse(JSON.stringify(channel)),
};

const connectionProfile = {
  name: "EBSI",
  version: "1.0.0",
  license: "Apache-2.0",
  client,
  channels,
  organizations,
  peers,
  certificateAuthorities,
  orderers,
};

module.exports = {
  ...sharedConf,
  ...finalConfig,
  port,
  username,
  password,
  connectionProfile,
};

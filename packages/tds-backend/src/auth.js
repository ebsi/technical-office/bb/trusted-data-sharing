const { utils } = require("@cef-ebsi/app-jwt");

const config = require("./config");
const { UnauthorizedError } = require("./errors");

function getToken(req) {
  const token = req.get("authorization");
  if (token) return token.replace("Bearer ", "");
  return null;
}

async function handleToken(req, res, next) {
  const token = getToken(req);

  if (!token) {
    next(new UnauthorizedError("Token not present in the headers"));
    return;
  }

  try {
    await utils.verify(token, {
      scope: "ebsi profile component",
      audience: config.jwt.acceptedApp,
      tarProvider: config.trustedAppsRegistry,
      expiration: {
        requestToken: 900,
      },
    });
    next();
  } catch (error) {
    next(error);
  }
}

module.exports = {
  handleToken,
  getToken,
};

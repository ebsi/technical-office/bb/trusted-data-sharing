const {
  DefaultEventHandlerStrategies,
  FileSystemWallet,
  Gateway,
} = require("fabric-network");
const config = require("./config");
const logger = require("./logger");

// Create a new file system based wallet for managing identities.
const { walletPath } = config;
const wallet = new FileSystemWallet(walletPath);

async function getUserFromWallet(userName) {
  logger.info(`Wallet path: ${walletPath}`);

  // Check to see if we've already enrolled the user.
  const userExists = await wallet.exists(userName);
  if (!userExists) {
    throw new Error(
      `An identity for the user ${userName} does not exist in the wallet`
    );
  }
  return true;
}

// Connect the gateway and get the contract
async function createContract(gateway, userName, request) {
  const connectOptions = {
    wallet,
    identity: userName,
    discovery: {
      enabled: false,
    },
    eventHandlerOptions: {
      commitTimeout: 120,
      strategy: DefaultEventHandlerStrategies.MSPID_SCOPE_ANYFORTX,
      //  Listen for transaction commit events from all peers in the client
      //  identity's organization. The submitTransaction function will wait
      //  until a successful event is received from any peer.
    },
  };
  logger.info("[~] Connecting to the gateway");
  await gateway.connect(config.connectionProfile, connectOptions);
  logger.info("[*] Connected to the gateway");

  // Get the network (channel) our contract is deployed to.
  logger.info(`[~] Initialized the network, ${config.channelName}`);
  const network = await gateway.getNetwork(config.channelName);
  logger.info(`[*] Initialized the network, ${config.channelName}`);

  // Get the contract from the network.
  logger.info(`[~] Get the contract", ${request.chaincodeId}`);
  const contract = network.getContract(request.chaincodeId);
  logger.info("[*] Got the contract");

  return contract;
}

// Invoke CC
async function invokeChaincode(userName, request) {
  try {
    logger.info("[*] getUserFromWallet");
    await getUserFromWallet(userName);

    // Create a new gateway for connecting to our peer node.
    logger.info("[*] gateway");
    const gateway = new Gateway();

    const contract = await createContract(gateway, userName, request);

    // Submit the specified transaction.
    logger.info("[~] contract.submitTransaction");
    const data = await contract.submitTransaction(request.fcn, ...request.args);
    logger.info("[*] Transaction has been submitted");
    logger.info("[*] <<>> Response: ", data.toString());

    // Disconnect from the gateway.
    await gateway.disconnect();
    return data.toString();
  } catch (error) {
    logger.error(`<<ERROR>> Failed to submit transaction: ${error}`);
    logger.error(`<<ERROR>> error.Error:`, error.toString());
    return Promise.reject(error);
  }
}

module.exports = {
  invokeChaincode,
};

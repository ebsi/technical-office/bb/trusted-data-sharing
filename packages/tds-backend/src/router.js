const express = require("express");
const bodyParser = require("body-parser");

const logger = require("./logger");
const { BadRequestError, InternalError } = require("./errors");
const controller = require("./controller");

const router = express.Router();

router.use(bodyParser.json({ limit: "10mb", extended: true, type: "*/*" }));

function checkParameters(method) {
  return (req, res, next) => {
    const { body } = req;
    const INVALID_PARAMETERS = "Invalid parameters";

    switch (method) {
      case "create_IossVat":
        if (
          !body.sharedID ||
          !body.iossVatID ||
          !body.startDate ||
          !body.endDate
        ) {
          next(new BadRequestError(INVALID_PARAMETERS));
          return;
        }
        break;

      case "update_IossVatPublicData":
        if (!body.sharedID || !body.startDate || !body.endDate) {
          next(new BadRequestError(INVALID_PARAMETERS));
          return;
        }
        break;

      case "update_IossVatPrivateData":
      case "verify_IossVatID":
        if (!body.sharedID || !body.iossVatID) {
          next(new BadRequestError(INVALID_PARAMETERS));
          return;
        }
        break;

      case "query_AllIossVatPublicData":
      case "query_AllIossVatPrivateData":
        break;

      case "query_IossVatPublicDataBySharedId":
      case "query_IossVatPrivateDataBySharedId":
        if (!body.sharedID) {
          next(new BadRequestError(INVALID_PARAMETERS));
          return;
        }
        break;

      default:
        logger.error(`method not found: ${method}`);
        next(new InternalError(`method not found: ${method}`));
        return;
    }
    next();
  };
}

router.post(
  "/create_IossVat",
  checkParameters("create_IossVat"),
  async (req, res, next) => {
    try {
      const result = await controller.createIossVat(req.body);
      res.send(result);
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/update_IossVatPublicData",
  checkParameters("update_IossVatPublicData"),
  async (req, res, next) => {
    try {
      const result = await controller.updateIossVatPublicData(req.body);
      res.send(result);
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/update_IossVatPrivateData",
  checkParameters("update_IossVatPrivateData"),
  async (req, res, next) => {
    try {
      const result = await controller.updateIossVatPrivateData(req.body);
      res.send(result);
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/verify_IossVatID",
  checkParameters("verify_IossVatID"),
  async (req, res, next) => {
    try {
      const result = await controller.verifyIossVatID(req.body);
      res.send(result);
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/query_AllIossVatPublicData",
  checkParameters("query_AllIossVatPublicData"),
  async (req, res, next) => {
    try {
      const result = await controller.queryAllIossVatPublicData(req.body);
      res.send(result);
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/query_AllIossVatPrivateData",
  checkParameters("query_AllIossVatPrivateData"),
  async (req, res, next) => {
    try {
      const result = await controller.queryAllIossVatPrivateData(req.body);
      res.send(result);
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/query_IossVatPublicDataBySharedId",
  checkParameters("query_IossVatPublicDataBySharedId"),
  async (req, res, next) => {
    try {
      const result = await controller.queryIossVatPublicDataBySharedId(
        req.body
      );
      res.send(result);
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/query_IossVatPrivateDataBySharedId",
  checkParameters("query_IossVatPrivateDataBySharedId"),
  async (req, res, next) => {
    try {
      const result = await controller.queryIossVatPrivateDataBySharedId(
        req.body
      );
      res.send(result);
    } catch (error) {
      next(error);
    }
  }
);

module.exports = router;

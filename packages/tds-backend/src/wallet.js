const FabricCAServices = require("fabric-ca-client");
const { FileSystemWallet, X509WalletMixin } = require("fabric-network");
const path = require("path");
const fs = require("fs");
const debugInit = require("debug")("init");

const config = require("./config");
const logger = require("./logger");

const cpp = config.connectionProfile;

const username = cpp.client.adminUser;
const password = cpp.client.adminPassword;

const orgName = Object.keys(cpp.organizations)[0];
const node = cpp.organizations[orgName].mspid;

const certName = Object.keys(cpp.certificateAuthorities)[0];
const certAuthority = cpp.certificateAuthorities[certName];

function saveCertificate(certificate, privKey, name) {
  logger.info(`Saving certificate of enrollment. Organization: ${name}`);
  const organization = cpp.organizations[name];

  // create folders if they don't exist
  const dirPrivKey = path.dirname(organization.adminPrivateKey.path);
  const dirCertificate = path.dirname(organization.signedCert.path);
  if (!fs.existsSync(dirPrivKey)) fs.mkdirSync(dirPrivKey, { recursive: true });
  if (!fs.existsSync(dirCertificate))
    fs.mkdirSync(dirCertificate, { recursive: true });

  // save certificate and privKey
  fs.writeFileSync(organization.signedCert.path, certificate);
  fs.writeFileSync(organization.adminPrivateKey.path, privKey);
}

async function init() {
  // Create a new file-system based wallet for the CA server admin
  const walletPath = path.resolve(__dirname, config.walletPath);
  const wallet = new FileSystemWallet(walletPath);

  // Check if the admin id exists in the wallet
  // TODO: check on server how many times the admin was enrolled! (In
  // production it can be only 1x)
  const adminExists = await wallet.exists(username);
  if (adminExists) {
    logger.info("Admin identity found in the wallet");
    return;
  }

  // Enroll the admin user, and import the new identity into the wallet.
  logger.info("Admin identity not found. Enrolling admin user");

  // Init a CA client for interacting with the CA server
  logger.info(`CertAuthority: ${JSON.stringify(certAuthority)}`);
  logger.info(`Node: ${node}`);
  const ca = new FabricCAServices(
    certAuthority.url,
    { verify: false },
    certAuthority.caName
  );

  // TODO: Check if we can set the admin CSR here.
  const enrollment = await ca.enroll({
    enrollmentID: username,
    enrollmentSecret: password,
  });
  const { certificate } = enrollment;
  const privKey = enrollment.key.toBytes();
  const identity = X509WalletMixin.createIdentity(node, certificate, privKey);
  debugInit("Identity: %o", identity);
  await wallet.import(username, identity);
  logger.info(
    `Successfully enrolled admin user ${username} and imported it into the wallet`
  );
  saveCertificate(certificate, privKey, orgName);
}

module.exports = {
  init,
};

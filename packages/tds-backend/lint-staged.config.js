module.exports = {
  "*.js": ["eslint --fix"],
  "*.{md,html,json,yml,yaml}": ["prettier --write"],
};

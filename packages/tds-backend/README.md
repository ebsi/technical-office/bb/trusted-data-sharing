![EBSI Logo](https://ec.europa.eu/cefdigital/wiki/images/logo/default-space-logo.svg)

# Trusted Data Sharing API

Trusted Data Sharing API for interaction with the Hyperledger Fabric Taxud chaincode.

## Installation

Create a `.env` file with a username and password. The API will enroll it in the certificate authority.

```
TDS_API_USERNAME=admin
TDS_API_PASSWORD=password
```

Fabric requires certificates to connect with the different peers. Add them into the following folders:

```
tds-backend/peerOrganizations/ebsinode1/tls/tlsintermediatecerts/tlsCa.pem
tds-backend/peerOrganizations/ebsinode2/tls/tlsintermediatecerts/tlsCa.pem
tds-backend/peerOrganizations/ebsinode3/tls/tlsintermediatecerts/tlsCa.pem
```

### Build from source

Install libraries and dependencies:

```sh
yarn install
```

Start the API:

```sh
EBSI_ENV=local yarn start
```

The API will be accesible at http://localhost:8080

## Tests

Create a copy of `.env.example` and name it `.env`. Define the corresponding variables and run:

```sh
yarn test
```

You can also run the unit and e2e tests separately:

```sh
yarn test:unit
yarn test:e2e
```

## Lint

Run:

```sh
yarn lint
```

## Licensing

Copyright (c) 2019 European Commission  
Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

- <https://joinup.ec.europa.eu/page/eupl-text-11-12>

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the Licence for the specific language governing permissions and limitations under the Licence.

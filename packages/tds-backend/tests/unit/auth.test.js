const auth = require("../../src/auth");

describe("test Auth functions", () => {
  it("check getToken function", async () => {
    expect.assertions(1);
    const t = "testToken";
    const token = auth.getToken({
      get: () => `Bearer ${t}`,
    });
    expect(token).toBe(t);
  });

  it("receive null in getToken", async () => {
    expect.assertions(1);
    const token = auth.getToken({
      get: () => "",
    });
    expect(token).toBeNull();
  });
});

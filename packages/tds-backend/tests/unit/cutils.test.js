const cutils = require("../../src/cutils");

describe("cutils function", () => {
  it("throw error for invalid username", async () => {
    expect.assertions(1);
    const check = async () => {
      await cutils.invokeChaincode("no-user", {});
    };
    await expect(check()).rejects.toThrow(Error);
  });
});

/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */
const ethers = require("ethers");
const crypto = require("crypto");
const didJWT = require("did-jwt");

function randomPrivateKey() {
  return `0x${crypto.randomBytes(32).toString("hex")}`;
}

// This function simulates a jwt issued by the wallet-api
function createEBSIjwt(data, pk) {
  let privateKey;
  if (pk) privateKey = pk.startsWith("0x") ? pk : `0x${pk}`;
  else privateKey = randomPrivateKey();
  const { address } = new ethers.Wallet(privateKey);
  const signer = didJWT.SimpleSigner(privateKey.replace("0x", ""));
  const headers = {
    issuer: `did:ebsi:${address}`,
    alg: "ES256K",
    signer,
  };
  return didJWT.createJWT(data, headers);
}

module.exports = {
  randomPrivateKey,
  createEBSIjwt,
};

/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */
const supertest = require("supertest");
const { v4: uuidv4 } = require("uuid");
const { Session } = require("@cef-ebsi/app-jwt");

const { url, TEST_APP_NAME, privKey } = require("../config");
const Server = require("../../src/server");

jest.setTimeout(30000);

let request;
let server = null;

if (url) {
  request = supertest(url);
} else {
  server = new Server().getServer();
  request = supertest(server);
}

const callTds = {
  post: (method, body) => {
    return request
      .post(`/tds/v1/iossvat${method}`)
      .set("Accept", "application/json")
      .send(body);
  },
  get: (method) => {
    return request
      .get(`/tds/v1/iossvat${method}`)
      .set("Accept", "application/json");
  },
};

let callTdsAuth;

const sharedID = uuidv4();
const iossVatID = "ABCDE1234567";
const startDate = "2020-01-18T00:00:00+01:00";
const endDate = "2021-01-18T00:00:00+01:00";

/*
 * Tests
 */

/* eslint jest/no-hooks: "off" */
describe("trusted data sharing integration test", () => {
  beforeAll(async () => {
    // Simulates a jwt issued by the wallet-api
    const session = new Session(TEST_APP_NAME, privKey);
    const result = await session.generateToken({
      did: "did:ebsi:0x7eF6aA722FB8aC4F8929eb242c91dDB85e9ea5a7",
      publicKey: "publickey",
      ticket: "ticket",
    });
    const jwt = result.accessToken;

    callTdsAuth = {
      post: (method, body) => {
        return request
          .post(`/tds/v1/iossvat${method}`)
          .set("Accept", "application/json")
          .set("Authorization", `Bearer ${jwt}`)
          .send(body);
      },
      get: (method) => {
        return request
          .get(`/tds/v1/iossvat${method}`)
          .set("Accept", "application/json")
          .set("Authorization", `Bearer ${jwt}`);
      },
    };
  });

  it("create iossvat", async () => {
    expect.assertions(0);
    await callTdsAuth
      .post("/create_IossVat", {
        sharedID,
        iossVatID,
        startDate,
        endDate,
      })
      .expect(200);
  });

  it("update iossvat public data", async () => {
    expect.assertions(0);
    await callTdsAuth
      .post("/update_IossVatPublicData", {
        sharedID,
        startDate,
        endDate,
      })
      .expect(200);
  });

  it("update iossvat private data", async () => {
    expect.assertions(0);
    await callTdsAuth
      .post("/update_IossVatPrivateData", {
        sharedID,
        iossVatID,
      })
      .expect(200);
  });

  it("verify iossvat id", async () => {
    expect.assertions(0);
    await callTdsAuth
      .post("/verify_IossVatID", {
        sharedID,
        iossVatID,
      })
      .expect(200);
  });

  it("query all ioss vat public data", async () => {
    expect.assertions(0);
    await callTdsAuth.get("/query_AllIossVatPublicData").expect(200);
  });

  it("query all ioss vat private data", async () => {
    expect.assertions(0);
    await callTdsAuth.get("/query_AllIossVatPrivateData").expect(200);
  });

  it("query public data by shared id", async () => {
    expect.assertions(0);
    await callTdsAuth
      .post("/query_IossVatPublicDataBySharedId", {
        sharedID,
      })
      .expect(200);
  });

  it("query private data by shared id", async () => {
    expect.assertions(0);
    await callTdsAuth
      .post("/query_IossVatPrivateDataBySharedId", {
        sharedID,
      })
      .expect(200);
  });

  // Invalid parameters

  it("throw invalid parameter for create iossvat", async () => {
    expect.assertions(0);
    await callTdsAuth.post("/create_IossVat", {}).expect(400);
  });

  it("throw invalid parameter for update iossvat public data", async () => {
    expect.assertions(0);
    await callTdsAuth.post("/update_IossVatPublicData", {}).expect(400);
  });

  it("throw invalid parameter for update iossvat private data", async () => {
    expect.assertions(0);
    await callTdsAuth.post("/update_IossVatPublicData", {}).expect(400);
  });

  it("throw invalid parameter for verify iossvat id", async () => {
    expect.assertions(0);
    await callTdsAuth.post("/verify_IossVatID", {}).expect(400);
  });

  it("throw invalid parameter for query public data by shared id", async () => {
    expect.assertions(0);
    await callTdsAuth
      .post("/query_IossVatPublicDataBySharedId", {})
      .expect(400);
  });

  it("throw invalid parameter for query private data by shared id", async () => {
    expect.assertions(0);
    await callTdsAuth
      .post("/query_IossVatPrivateDataBySharedId", {})
      .expect(400);
  });

  // Invalid token and method

  it("throws unauthorized error when the token is not present", async () => {
    expect.assertions(0);
    await callTds.get("/some-method").expect(401);
  });

  it("throws bad request error when for unknown method", async () => {
    expect.assertions(0);
    await callTdsAuth.get("/unknown-method").expect(400);
  });
});

![EBSI Logo](https://ec.europa.eu/cefdigital/wiki/images/logo/default-space-logo.svg)

# Trusted Data Sharing UI

Trusted Data Sharing Demo for interaction with the Hyperledger Fabric.

## Build

To install the dependencies, run:

```sh
yarn install
```

To build the webpage for production, run:

```sh
yarn build
```

## Lint

Run:

```sh
yarn lint
```

## Test

Run:

```sh
yarn test
```

## Licensing

Copyright (c) 2019 European Commission  
Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

- <https://joinup.ec.europa.eu/page/eupl-text-11-12>

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the Licence for the specific language governing permissions and limitations under the Licence.

module.exports = {
  "*.{js,jsx}": ["eslint --fix"],
  "*.{css,scss}": ["stylelint --fix"],
  "*.{md,html,json,yml,yaml}": ["prettier --write"],
};

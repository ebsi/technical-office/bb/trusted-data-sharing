import React, { useContext, useState } from "react";
import { Redirect, useLocation } from "react-router-dom";
import { AuthContext, LOGIN_CODES } from "../Auth/Auth";
import PageHeader from "../PageHeader/PageHeader";

function Login() {
  const { isAuthenticated, login } = useContext(AuthContext);
  const [errorDuringLogin, setErrorDuringLogin] = useState(LOGIN_CODES.SUCCESS);
  const location = useLocation();

  if (isAuthenticated) {
    if (location && location.state && location.state.from) {
      return <Redirect to={location.state.from} />;
    }

    return <Redirect to="/" />;
  }

  function handleLogin() {
    const loginCode = login();
    if (loginCode !== LOGIN_CODES.SUCCESS) {
      setErrorDuringLogin(loginCode);
    }
  }

  return (
    <>
      <PageHeader>Log in</PageHeader>
      <main className="ecl-container ecl-u-mv-xl" data-testid="main-login">
        <p>
          This pages acts as a demonstrator to simulate the Trusted Identity
          Provider. During the login, we will check that you have correctly
          followed the EBSI onboarding process. If the verification fails, you
          will be redirected to the onboarding page.
        </p>
        <button
          type="button"
          data-testid="button"
          className="ecl-button ecl-button--primary"
          onClick={handleLogin}
        >
          Log in
        </button>
        {(() => {
          if (errorDuringLogin === LOGIN_CODES.MALFORMED_JWT) {
            return (
              <div className="invalid-feedback" style={{ display: "block" }}>
                Error during login: JWT is malformed, parsing failed.
              </div>
            );
          }
          if (errorDuringLogin === LOGIN_CODES.MISSING_JWT) {
            return (
              <div className="invalid-feedback" style={{ display: "block" }}>
                Error during login: JWT not available or empty. Please make sure
                you are authenticated with the wallet.
              </div>
            );
          }
          if (errorDuringLogin === LOGIN_CODES.MISSING_PROPS_JWT) {
            return (
              <div className="invalid-feedback" style={{ display: "block" }}>
                Error during login: your JWT is missing some essential
                properties. Please try to log in again in the wallet.
              </div>
            );
          }
          if (errorDuringLogin === LOGIN_CODES.EXPIRED_JWT) {
            return (
              <div className="invalid-feedback" style={{ display: "block" }}>
                Error during login: your JWT has expired. Please log in again in
                the wallet.
              </div>
            );
          }
          return null;
        })()}
      </main>
    </>
  );
}

export default Login;

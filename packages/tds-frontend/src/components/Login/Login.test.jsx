import React from "react";
import ReactDOM from "react-dom";
import { Router } from "react-router-dom";
import { render, cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { createMemoryHistory } from "history";
import { JWT, JWK } from "jose";

import Login from "./Login";
import Auth from "../Auth/Auth";

const key = JWK.asKey({
  kty: "oct",
  k: "hJtXIZ2uSN5kbQfbtTNWbpdmhkV8FJG-Onbc6mxCcYg",
});

const setup = () => {
  const utils = render(
    <Router history={createMemoryHistory()}>
      <Auth>
        <Login />
      </Auth>
    </Router>
  );
  const { getByTestId } = utils;
  const loginButton = getByTestId("button");
  const mainLogin = getByTestId("main-login");
  return {
    ...utils,
    loginButton,
    mainLogin,
  };
};

/* eslint jest/no-hooks: "off" */
describe("login", () => {
  beforeEach(() => {
    localStorage.clear();
  });

  afterEach(cleanup);

  it("render login without crashing", () => {
    expect.assertions(0);
    const history = createMemoryHistory({ initialEntries: ["/test"] });

    const div = document.createElement("div");
    ReactDOM.render(
      <Router history={history}>
        <Login />
      </Router>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });

  it("error missing Jwt", () => {
    expect.assertions(1);
    const { loginButton, mainLogin } = setup();
    fireEvent.click(loginButton);

    expect(mainLogin).toHaveTextContent(
      "Error during login: JWT not available"
    );
  });

  it("error malformed Jwt", () => {
    expect.assertions(1);
    const { loginButton, mainLogin } = setup();
    localStorage.setItem("Jwt", "123");
    fireEvent.click(loginButton);

    expect(mainLogin).toHaveTextContent("Error during login: JWT is malformed");
  });

  it("error Jwt missing property", () => {
    expect.assertions(1);
    const { loginButton, mainLogin } = setup();

    const token = JWT.sign({}, key, {
      expiresIn: "2 hours",
      header: { typ: "JWT" },
      subject: "test",
    });
    localStorage.setItem("Jwt", token);

    fireEvent.click(loginButton);

    expect(mainLogin).toHaveTextContent(
      "Error during login: your JWT is missing some essential properties"
    );
  });

  it("error Jwt expired", () => {
    expect.assertions(1);
    const { loginButton, mainLogin } = setup();

    const token = JWT.sign(
      {
        sub_jwk: {
          kid: "did:ebsi:0x1234",
        },
        exp: 1577836800,
        aud: "tds",
      },
      key,
      {
        header: { typ: "JWT" },
        subject: "test",
      }
    );
    localStorage.setItem("Jwt", token);

    fireEvent.click(loginButton);

    expect(mainLogin).toHaveTextContent(
      "Error during login: your JWT has expired"
    );
  });

  it("success login with Jwt", () => {
    expect.assertions(0);
    const { loginButton } = setup();

    const token = JWT.sign(
      {
        sub_jwk: {
          kid: "did:ebsi:0x1234",
        },
      },
      key,
      {
        expiresIn: "2 hours",
        header: { typ: "JWT" },
        subject: "test",
        aud: "tds",
      }
    );
    localStorage.setItem("Jwt", token);

    fireEvent.click(loginButton);
  });
});

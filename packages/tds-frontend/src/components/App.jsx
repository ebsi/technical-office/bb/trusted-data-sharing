import React from "react";
import { Switch, Route, BrowserRouter, Redirect } from "react-router-dom";
import Auth from "./Auth/Auth";
import Main from "./Main/Main";
import Login from "./Login/Login";
import config from "../config";
import PrivateRoute from "./PrivateRoute/PrivateRoute";
import ScrollToTop from "./ScrollToTop/ScrollToTop";
import Layout from "./Layout/Layout";

const publicUrl = config.apps.tds;

function App() {
  return (
    <div className="App">
      <Auth>
        <BrowserRouter basename={publicUrl}>
          <ScrollToTop />
          <Layout>
            <Switch>
              <Route exact path="/login" component={Login} />
              <PrivateRoute exact path="/" component={Main} />
              <Route path="*">
                <Redirect to="/" />
              </Route>
            </Switch>
          </Layout>
        </BrowserRouter>
      </Auth>
    </div>
  );
}

export default App;

/* eslint-disable no-console */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { Formik, Form, Field } from "formik";
import axios from "axios";
import { parse, formatRFC3339 } from "date-fns";
import { v4 as uuid } from "uuid";
import classnames from "classnames";
import PageHeader from "../PageHeader/PageHeader";
import config from "../../config";
import icons from "../../assets/images/icons.svg";

function getJWT() {
  const token = localStorage.getItem("Jwt");
  if (!token) throw new Error("No JWT in the local storage");
  return token;
}

function headersJWT() {
  return { Authorization: `Bearer ${getJWT()}` };
}

function validateVatID(value) {
  if (!value) {
    return "This field is required";
  }

  if (value.length < 12) {
    return "Too short (expected length: 12 chars)";
  }

  if (value.length > 12) {
    return "Too long (expected length: 12 chars)";
  }

  if (!/^([A-Z]{5})(\d{7})$/.test(value)) {
    return 'IOSS VAT ID has the wrong format. Expected ("ABCDE1234567")';
  }

  return "";
}

function validateDate(value) {
  if (!value) {
    return "This field is required";
  }

  try {
    const result = parse(value, "dd/MM/yyyy", new Date());
    formatRFC3339(result);
  } catch (err) {
    return 'Date has the wrong format. Expected ("dd/mm/yyyy")';
  }

  return "";
}

function isRequired(value) {
  if (!value) {
    return "This field is required";
  }

  return "";
}

function hasClass(el, className) {
  if (el.classList) return el.classList.contains(className);
  return !!el.className.match(new RegExp(`(\\s|^)${className}(\\s|$)`));
}

function addClass(element, className) {
  const el = element;
  if (el.classList) el.classList.add(className);
  else if (!hasClass(el, className)) el.className += ` ${className}`;
}

function removeClass(element, className) {
  const el = element;
  if (el.classList) el.classList.remove(className);
  else if (hasClass(el, className)) {
    const reg = new RegExp(`(\\s|^)${className}(\\s|$)`);
    el.className = el.className.replace(reg, " ");
  }
}

function createIossVat(sharedID, iossVatID, startDate, endDate) {
  try {
    const formatStartDate = formatRFC3339(
      parse(startDate, "dd/MM/yyyy", new Date())
    );
    const formatEndDate = formatRFC3339(
      parse(endDate, "dd/MM/yyyy", new Date())
    );
    return axios({
      method: "post",
      url: `${config.apis.tds}/create_IossVat/`,
      data: {
        sharedID,
        iossVatID,
        startDate: formatStartDate,
        endDate: formatEndDate,
      },
      headers: headersJWT(),
    });
  } catch (error) {
    console.log(error);
  }
  return null;
}

function verifyIossVat(sharedID, iossVatID) {
  return axios({
    method: "post",
    url: `${config.apis.tds}/verify_IossVatID/`,
    data: {
      sharedID,
      iossVatID,
    },
    headers: headersJWT(),
  });
}

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      displayDisclaimer: true,
    };
  }

  componentDidMount() {}

  onSubmitVatDetailForm = (formData) => {
    // encrypt data here; generation of sharedID can happen here.
    const sharedID = uuid();

    return createIossVat(
      sharedID,
      formData.vatID,
      formData.startDate,
      formData.endDate
    )
      .then(() => {
        removeClass(
          document.getElementById("labelVatDetailSuccess"),
          "ecl-u-type-color-red"
        );
        addClass(
          document.getElementById("labelVatDetailSuccess"),
          "ecl-u-type-color-green"
        );
        document.getElementById(
          "labelVatDetailSuccess"
        ).innerHTML = `Your public Shared ID for this Detail is: ${sharedID}. You can use this Shared ID to verify the registered IOSS VAT ID`;
      })
      .catch((e) => {
        // TODO: replace "alert" with a custom error handling behavior, like displaying a toast or an error message on the page
        // alert(e.message);
        removeClass(
          document.getElementById("labelVatDetailSuccess"),
          "ecl-u-type-color-green"
        );
        addClass(
          document.getElementById("labelVatDetailSuccess"),
          "ecl-u-type-color-red"
        );
        document.getElementById("labelVatDetailSuccess").innerHTML =
          "Failed to create/update IOSS VAT ID. Check the console for more  details.";
        console.error("[ERROR]", e.message);
      });
  };

  onSubmitVerifyForm = (formData) => {
    // encrypt data here; generation of sharedID can happen here.
    // const sharedID = uuid();

    return verifyIossVat(formData.verifySharedID, formData.verifyIOSSID)
      .then(() => {
        removeClass(
          document.getElementById("labelVerifySuccess"),
          "ecl-u-type-color-red"
        );
        addClass(
          document.getElementById("labelVerifySuccess"),
          "ecl-u-type-color-green"
        );
        document.getElementById("labelVerifySuccess").innerHTML =
          "The IOSS VAT ID is valid";
      })
      .catch((e) => {
        removeClass(
          document.getElementById("labelVerifySuccess"),
          "ecl-u-type-color-green"
        );
        addClass(
          document.getElementById("labelVerifySuccess"),
          "ecl-u-type-color-red"
        );
        document.getElementById("labelVerifySuccess").innerHTML =
          "The IOSS VAT ID is not valid";
        console.error("[ERROR]", e.message);
      });
  };

  render() {
    const {
      // successToast,
      displayDisclaimer /* credential, username */,
    } = this.state;
    return (
      <>
        <PageHeader>Trusted Data Sharing demo</PageHeader>
        <main className="ecl-container ecl-u-mv-xl">
          <div className="ecl-row ecl-u-mt-l">
            <div className="ecl-col-lg-3">
              <nav
                className="ecl-inpage-navigation"
                data-ecl-inpage-navigation
                data-ecl-auto-init="InpageNavigation"
              >
                <div className="ecl-inpage-navigation__title">
                  Page contents
                </div>
                <div className="ecl-inpage-navigation__body">
                  <button
                    type="button"
                    className="ecl-inpage-navigation__trigger"
                    id="ecl-inpage-navigation-trigger"
                    data-ecl-inpage-navigation-trigger="true"
                    aria-controls="ecl-inpage-navigation-list"
                    aria-expanded="false"
                  >
                    <span
                      className="ecl-inpage-navigation__trigger-current"
                      data-ecl-inpage-navigation-trigger-current="true"
                    >
                      &nbsp;
                    </span>
                    <svg
                      focusable="false"
                      aria-hidden="true"
                      className="ecl-inpage-navigation__trigger-icon ecl-icon ecl-icon--s ecl-icon--rotate-180"
                    >
                      <use xlinkHref={`${icons}#ui--corner-arrow`} />
                    </svg>
                  </button>
                  <ul
                    className="ecl-inpage-navigation__list"
                    hidden=""
                    aria-labelledby="ecl-inpage-navigation-trigger"
                    data-ecl-inpage-navigation-list="true"
                    id="ecl-inpage-navigation-list"
                  >
                    <li className="ecl-inpage-navigation__item">
                      <a
                        data-ecl-inpage-navigation-link="true"
                        href="#scenario1"
                        className="ecl-inpage-navigation__link ecl-link ecl-link--standalone"
                      >
                        Scenario 1: Publication of IOSS VAT ID
                      </a>
                    </li>
                    <li className="ecl-inpage-navigation__item">
                      <a
                        data-ecl-inpage-navigation-link="true"
                        href="#scenario2"
                        className="ecl-inpage-navigation__link ecl-link ecl-link--standalone"
                      >
                        Scenario 2: Verification of IOSS VAT ID
                      </a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
            <div className="ecl-col-lg-9">
              {displayDisclaimer && (
                <div role="alert" className="ecl-message ecl-message--info">
                  <svg
                    focusable="false"
                    aria-hidden="true"
                    className="ecl-message__icon ecl-icon ecl-icon--l"
                  >
                    <use xlinkHref={`${icons}#notifications--information`} />
                  </svg>
                  <div className="ecl-message__content">
                    <button
                      data-ecl-message-close="true"
                      type="button"
                      className="ecl-message__close ecl-button ecl-button--ghost"
                      onClick={() => {
                        this.setState({ displayDisclaimer: false });
                      }}
                    >
                      <span className="ecl-button__container">
                        <span
                          className="ecl-button__label"
                          data-ecl-label="true"
                        >
                          Close
                        </span>
                        <svg
                          focusable="false"
                          aria-hidden="true"
                          data-ecl-icon="true"
                          className="ecl-button__icon ecl-button__icon--after ecl-icon ecl-icon--s"
                        >
                          <use xlinkHref={`${icons}#ui--close`} />
                        </svg>
                      </span>
                    </button>
                    <div className="ecl-message__title">Disclaimer</div>
                    <p className="ecl-message__description">
                      This is a demo website to show the technical capabilities
                      of the EBSI project. We use dummy data! All the public
                      entities are simulated, there is no real interaction with
                      any of them.
                    </p>
                  </div>
                </div>
              )}
              <p className="ecl-u-type-paragraph ecl-u-type-justify ecl-u-mt-xl">
                In the scope of the EBSI Use case “Trusted Data Sharing”, we
                built here under as illustration a demonstrator Webapp for the
                IOSS VAT ID sharing project, using EBSI infrastructure. Taxation
                authorities of the MS of Identification submit data (IOSS VAT
                ID) for publication (Scenario 1), while Customs authorities of
                the other MS can access published data to check the validity of
                the IOSS VAT identification numbers declared in import
                declarations at import time (Scenario 2)
              </p>
              <h2 className="ecl-u-type-heading-2 ecl-u-mt-2xl" id="scenario1">
                Scenario 1: Publication of IOSS VAT ID by Taxation Authorities
                of Identification MS
              </h2>
              <Formik
                initialValues={{ vatID: "", startDate: "", endDate: "" }}
                onSubmit={(values, { setSubmitting }) => {
                  this.onSubmitVatDetailForm(values).then(() => {
                    setSubmitting(false);
                  });
                }}
              >
                {({ isSubmitting }) => (
                  <Form>
                    <Field name="vatID" validate={validateVatID}>
                      {({ field, meta }) => (
                        <div
                          id="formVat-vatID"
                          className="ecl-form-group ecl-form-group--text-input ecl-u-mt-l"
                        >
                          <label className="ecl-form-label" htmlFor="formVatID">
                            IOSS VAT ID
                          </label>
                          <div className="ecl-help-block">
                            In case the ID already exists, this will update the
                            Start and End date.
                          </div>
                          {meta.touched && meta.error && (
                            <div className="ecl-feedback-message">
                              {meta.error}
                            </div>
                          )}
                          <input
                            type="text"
                            placeholder="Enter unique IOSS VAT ID (Ex: IMABC1234567)"
                            id="formVatID"
                            className={classnames(
                              "ecl-text-input ecl-text-input--l",
                              {
                                "ecl-text-input--invalid":
                                  meta.touched && meta.error,
                              }
                            )}
                            {...field}
                          />
                        </div>
                      )}
                    </Field>
                    <Field name="startDate" validate={validateDate}>
                      {({ field, meta }) => (
                        <div
                          id="formVat-startDate"
                          className="ecl-form-group ecl-form-group--text-input ecl-u-mt-m"
                        >
                          <label
                            className="ecl-form-label"
                            htmlFor="formStartDate"
                          >
                            Start of Validity Period
                          </label>
                          {meta.touched && meta.error && (
                            <div className="ecl-feedback-message">
                              {meta.error}
                            </div>
                          )}
                          <input
                            type="text"
                            id="formStartDate"
                            className={classnames(
                              "ecl-text-input ecl-text-input--l",
                              {
                                "ecl-text-input--invalid":
                                  meta.touched && meta.error,
                              }
                            )}
                            placeholder="Enter Start of Validity Period (Ex: 18/01/2020)"
                            {...field}
                          />
                        </div>
                      )}
                    </Field>
                    <Field name="endDate" validate={validateDate}>
                      {({ field, meta }) => (
                        <div
                          id="formVat-endDate"
                          className="ecl-form-group ecl-form-group--text-input ecl-u-mt-m"
                        >
                          <label
                            className="ecl-form-label"
                            htmlFor="formEndDate"
                          >
                            End of Validity Period
                          </label>
                          {meta.touched && meta.error && (
                            <div className="ecl-feedback-message">
                              {meta.error}
                            </div>
                          )}
                          <input
                            type="text"
                            id="formEndDate"
                            className={classnames(
                              "ecl-text-input ecl-text-input--l",
                              {
                                "ecl-text-input--invalid":
                                  meta.touched && meta.error,
                              }
                            )}
                            placeholder="Enter End of Validity Period (Ex: 18/04/2021)"
                            {...field}
                          />
                        </div>
                      )}
                    </Field>
                    <div className="ecl-form-group ecl-form-group--text-input ecl-u-mt-m">
                      <p
                        id="labelVatDetailSuccess"
                        className="ecl-form-label ecl-u-type-color-green"
                        htmlFor="btnVatDetail"
                      />
                      <button
                        id="btnVatDetail"
                        type="submit"
                        className="ecl-button ecl-button--primary ecl-u-mt-m"
                        disabled={isSubmitting}
                      >
                        {isSubmitting ? "Sending..." : "Create the IOSS VAT ID"}
                      </button>
                    </div>
                  </Form>
                )}
              </Formik>
              <h2 className="ecl-u-type-heading-2 ecl-u-mt-2xl" id="scenario2">
                Scenario 2: Verification of IOSS VAT ID by Customs authorities
                of other MS
              </h2>
              <Formik
                initialValues={{ verifySharedID: "", verifyIOSSID: "" }}
                onSubmit={(values, { setSubmitting }) => {
                  this.onSubmitVerifyForm(values).then(() => {
                    setSubmitting(false);
                  });
                }}
              >
                {({ isSubmitting }) => (
                  <Form>
                    <Field name="verifySharedID" validate={isRequired}>
                      {({ field, meta }) => (
                        <div className="ecl-form-group ecl-form-group--text-input ecl-u-mt-l">
                          <label
                            className="ecl-form-label"
                            htmlFor="formVerifySharedID"
                          >
                            Shared ID
                          </label>
                          {meta.touched && meta.error && (
                            <div className="ecl-feedback-message">
                              {meta.error}
                            </div>
                          )}
                          <input
                            type="text"
                            id="formVerifySharedID"
                            className={classnames(
                              "ecl-text-input ecl-text-input--l",
                              {
                                "ecl-text-input--invalid":
                                  meta.touched && meta.error,
                              }
                            )}
                            placeholder="Enter Shared ID"
                            {...field}
                          />
                          <label>
                            Shared ID is a temporary Public Field linked to an
                            IOSS VAT ID that is stored on privacy mode
                          </label>
                        </div>
                      )}
                    </Field>
                    <Field name="verifyIOSSID" validate={validateVatID}>
                      {({ field, meta }) => (
                        <div className="ecl-form-group ecl-form-group--text-input ecl-u-mt-m">
                          <label
                            className="ecl-form-label"
                            htmlFor="formVerifyIOSSID"
                          >
                            IOSS VAT ID
                          </label>
                          {meta.touched && meta.error && (
                            <div className="ecl-feedback-message">
                              {meta.error}
                            </div>
                          )}
                          <input
                            type="text"
                            id="formVerifyIOSSID"
                            className={classnames(
                              "ecl-text-input ecl-text-input--l",
                              {
                                "ecl-text-input--invalid":
                                  meta.touched && meta.error,
                              }
                            )}
                            placeholder="Enter unique IOSS VAT ID (Ex: IMABC1234567)"
                            {...field}
                          />
                        </div>
                      )}
                    </Field>
                    <div className="ecl-form-group ecl-form-group--text-input ecl-u-mt-m">
                      <label
                        id="labelVerifySuccess"
                        className="ecl-form-label ecl-u-type-color-green"
                        htmlFor="btnValidate"
                      />
                      <button
                        id="btnValidate"
                        type="submit"
                        className="ecl-button ecl-button--primary ecl-u-mt-m"
                      >
                        {isSubmitting
                          ? "Verifying..."
                          : "Verify Validity of IOSS VAT ID"}
                      </button>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </main>
      </>
    );
  }
}

export default Main;

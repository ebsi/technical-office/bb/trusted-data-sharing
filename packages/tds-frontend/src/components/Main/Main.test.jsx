import React from "react";
import ReactDOM from "react-dom";
import { Router } from "react-router-dom";
import { render, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { createMemoryHistory } from "history";
import { JWT, JWK } from "jose";
import axios from "axios";

import Main from "./Main";

jest.mock("axios");

const key = JWK.asKey({
  kty: "oct",
  k: "hJtXIZ2uSN5kbQfbtTNWbpdmhkV8FJG-Onbc6mxCcYg",
});

const token = JWT.sign({}, key, {
  expiresIn: "2 hours",
  header: { typ: "JWT" },
  subject: "test",
});

localStorage.setItem("Jwt", token);

const setup = () => {
  const utils = render(
    <Router history={createMemoryHistory()}>
      <Main />
    </Router>
  );
  const { container } = utils;
  const createButton = container.querySelector("#btnVatDetail");
  const verifyButton = container.querySelector("#btnValidate");
  return {
    ...utils,
    createButton,
    verifyButton,
  };
};

describe("main component", () => {
  it("render main component", () => {
    expect.assertions(0);
    const div = document.createElement("div");
    ReactDOM.render(
      <Router history={createMemoryHistory()}>
        <Main />
      </Router>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });

  it("fails with empty fields", async () => {
    expect.assertions(1);
    const { createButton, container } = setup();

    fireEvent.click(createButton);
    await waitFor(() => container);
    const el = container.querySelector("#formVat-vatID");
    expect(el).toHaveTextContent("This field is required");
  });

  it("fails with short vatID", async () => {
    expect.assertions(1);
    const { createButton, container } = setup();

    const inputVatId = container.querySelector("#formVatID");
    fireEvent.change(inputVatId, { target: { value: "123" } });

    fireEvent.click(createButton);
    await waitFor(() => container);
    const el = container.querySelector("#formVat-vatID");
    expect(el).toHaveTextContent("Too short (expected length: 12 chars)");
  });

  it("fails with long vatID", async () => {
    expect.assertions(1);
    const { createButton, container } = setup();

    const inputVatId = container.querySelector("#formVatID");
    fireEvent.change(inputVatId, { target: { value: "IMABDC123456789123" } });

    fireEvent.click(createButton);
    await waitFor(() => container);

    const el = container.querySelector("#formVat-vatID");
    expect(el).toHaveTextContent("Too long (expected length: 12 chars)");
  });

  it("fails with invalid vatID", async () => {
    expect.assertions(1);
    const { createButton, container } = setup();

    const inputVatId = container.querySelector("#formVatID");
    fireEvent.change(inputVatId, { target: { value: "$$$IMABDC123" } });

    fireEvent.click(createButton);
    await waitFor(() => container);

    const el = container.querySelector("#formVat-vatID");
    expect(el).toHaveTextContent("IOSS VAT ID has the wrong format");
  });

  it("fails with invalid date", async () => {
    expect.assertions(1);
    const { createButton, container } = setup();

    const inputStartDate = container.querySelector("#formStartDate");
    fireEvent.change(inputStartDate, { target: { value: "Jan 3, 1988" } });

    fireEvent.click(createButton);
    await waitFor(() => container);

    const el = container.querySelector("#formVat-startDate");
    expect(el).toHaveTextContent("Date has the wrong format");
  });

  it("create iossvat", async () => {
    expect.hasAssertions();
    const { createButton, container } = setup();

    const inputVatId = container.querySelector("#formVatID");
    fireEvent.change(inputVatId, { target: { value: "IMABD1234567" } });

    const inputStartDate = container.querySelector("#formStartDate");
    fireEvent.change(inputStartDate, { target: { value: "01/01/2019" } });

    const inputEndDate = container.querySelector("#formEndDate");
    fireEvent.change(inputEndDate, { target: { value: "01/01/2029" } });

    axios.mockImplementation(() => Promise.resolve({ status: 200, data: "" }));
    fireEvent.click(createButton);

    await waitFor(() => expect(axios).toHaveBeenCalledTimes(1));

    const el = container.querySelector("#labelVatDetailSuccess");
    expect(el).toHaveTextContent("Your public Shared ID for this Detail is");
  });

  it("verify iossvat", async () => {
    expect.hasAssertions();
    const { verifyButton, container } = setup();

    const inputSharedId = container.querySelector("#formVerifySharedID");
    fireEvent.change(inputSharedId, {
      target: { value: "1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed" },
    });

    const inputVatId = container.querySelector("#formVerifyIOSSID");
    fireEvent.change(inputVatId, { target: { value: "IMABD1234567" } });

    axios.mockImplementation(() => Promise.resolve({ status: 200, data: "" }));
    fireEvent.click(verifyButton);

    await waitFor(() => expect(axios).toHaveBeenCalledTimes(1));

    const el = container.querySelector("#labelVerifySuccess");
    expect(el).toHaveTextContent("The IOSS VAT ID is valid");
  });

  it("display error for invalid creation", async () => {
    expect.hasAssertions();
    const { createButton, container } = setup();

    const inputVatId = container.querySelector("#formVatID");
    fireEvent.change(inputVatId, { target: { value: "IMABD1234567" } });

    const inputStartDate = container.querySelector("#formStartDate");
    fireEvent.change(inputStartDate, { target: { value: "01/01/2019" } });

    const inputEndDate = container.querySelector("#formEndDate");
    fireEvent.change(inputEndDate, { target: { value: "01/01/2029" } });

    axios.mockImplementation(() =>
      Promise.reject(new Error("display error for invalid creation"))
    );
    fireEvent.click(createButton);

    await waitFor(() => expect(axios).toHaveBeenCalledTimes(3));

    const el = container.querySelector("#labelVatDetailSuccess");
    expect(el).toHaveTextContent("Failed to create/update IOSS VAT ID");
  });

  it("display error for invalid verification", async () => {
    expect.hasAssertions();
    const { verifyButton, container } = setup();

    const inputSharedId = container.querySelector("#formVerifySharedID");
    fireEvent.change(inputSharedId, {
      target: { value: "1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed" },
    });

    const inputVatId = container.querySelector("#formVerifyIOSSID");
    fireEvent.change(inputVatId, { target: { value: "IMABD1234567" } });

    axios.mockImplementation(() =>
      Promise.reject(new Error("display error for invalid verification"))
    );
    fireEvent.click(verifyButton);

    await waitFor(() => expect(axios).toHaveBeenCalledTimes(4));

    const el = container.querySelector("#labelVerifySuccess");
    expect(el).toHaveTextContent("The IOSS VAT ID is not valid");
  });
});

import React from "react";
import PropTypes from "prop-types";
import styles from "./Layout.module.css";
import config from "../../config";

export default function Layout({ children }) {
  return (
    <div className={styles.root}>
      {children}
      <div className={styles.ribbon}>
        <a className={styles.ribbonText} href={config.apps.demo}>
          EBSI DEMO
        </a>
      </div>
    </div>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

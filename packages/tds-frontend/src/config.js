const config = {
  production: {
    domain: "ebsi.tech.ec.europa.eu",
  },
  development: {
    domain: "ebsi.xyz",
  },
  integration: {
    domain: "intebsi.xyz",
  },
  local: {
    domain: "intebsi.xyz",
  },
};

const environment = process.env.REACT_APP_ENV || "local";
const finalConfig = config[environment];
const url = {
  api: `https://api.${finalConfig.domain}`,
  app: `https://app.${finalConfig.domain}`,
};

const sharedConf = {
  apis: {
    tds: `${url.api}/tds/v1/iossvat`,
  },
  apps: {
    tds: "/demo/trusted-data-sharing",
    demo: "/demo",
  },
};

if (process.env.REACT_APP_API_TDS)
  sharedConf.apis.tds = `${process.env.REACT_APP_API_TDS}/tds/v1/iossvat`;

export default {
  ...finalConfig,
  ...sharedConf,
};

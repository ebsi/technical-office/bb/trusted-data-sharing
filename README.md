![EBSI Logo](https://ec.europa.eu/cefdigital/wiki/images/logo/default-space-logo.svg)

# Trusted Data Sharing

This repository contains the code of a Trusted Data Sharing sample application. In the `packages/` folder, you will find the backend (`tds-backend/`) and the frontend (`tds-frontend/`), created as 2 distinct entities.

The backend is based on NodeJS. The frontend uses Create-React-App.

In development, you can run both project separately. Refer to their own documentation for more information.

In production, the build of Create-React-App should be served from the `public/` folder, inside the backend. For convenience, you can use Docker Compose and the provided Dockerfile to automatically build and run the frontend and backend with 1 command. See the "Run with Docker Compose" section below for more information.

## Run with Docker Compose

Before starting Docker Compose, create a copy of `.env.example` and name it `.env`. Set the environment variables accordingly. From now on, we'll consider that you're using the default environement variables.

Fabric requires certificates to connect with the different peers. Add them into the following folders:

```
trusted-data-sharing/peerOrganizations/ebsinode1/tls/tlsintermediatecerts/tlsCa.pem
trusted-data-sharing/peerOrganizations/ebsinode2/tls/tlsintermediatecerts/tlsCa.pem
trusted-data-sharing/peerOrganizations/ebsinode3/tls/tlsintermediatecerts/tlsCa.pem
```

Now, run:

```sh
docker-compose up --build
```

And open http://localhost:8080/demo/trusted-data-sharing to see the results.

## Licensing

Copyright (c) 2019 European Commission
Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

- <https://joinup.ec.europa.eu/page/eupl-text-11-12>

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the Licence for the specific language governing permissions and limitations under the Licence.
